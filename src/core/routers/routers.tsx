import { createBrowserRouter } from "react-router-dom";
import { Screen, ScreenPath } from "../../main_screen";

export const router = createBrowserRouter([
  {
    path: ScreenPath,
    element: <Screen />,
  },
]);
