export default {
	extends: ['stylelint-config-recommended-vue', 'stylelint-config-tailwindcss'],
	plugins: [], // Убедитесь, что плагинов нет, если они не требуются
	rules: {
		'at-rule-no-unknown': [
			true,
			{
				ignoreAtRules: ['tailwind', 'apply', 'layer', 'variants', 'responsive'],
			},
		],
		'no-empty-source': null,
		'prettier/prettier': true,
	},
}
