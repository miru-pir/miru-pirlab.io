const siteUrl = 'https://miru-pir.ru'
export default defineNuxtConfig({
	telemetry: false,
	ssr: true, // Серверный рендеринг включён (по умолчанию)
	compatibilityDate: '2024-11-01',
	devtools: { enabled: true },
	components: {
		dirs: ['~/components'],
	},
	modules: ['@nuxtjs/tailwindcss', '@nuxtjs/i18n', 'nuxt-icons'],

	i18n: {
		locales: [
			{ code: 'ru', iso: 'ru-RU', file: 'ru.json', name: 'Русский' },
			{ code: 'en', iso: 'en-US', file: 'en.json', name: 'English' },
		],
		defaultLocale: 'ru',
		langDir: 'locales/',
		strategy: 'no_prefix',
		lazy: true,
	},
	app: {
		// Базовый URL приложения делаем относительным
		// baseURL: './',
		// Кастомная папка для сборки ассетов (_nuxt)
		// buildAssetsDir: './_nuxt/',
		head: {
			htmlAttrs: { lang: 'ru_RU' },
			title: 'Платформа инновационного развития',
			meta: [
				{
					name: 'description',
					content:
						'Сообщество единомышленников, объединённых для создания и продвижения передовых инженерных и технологических решений',
				},
				{ property: 'og:type', content: 'website' },
				{ property: 'og:site_name', content: 'Платформа инновационного развития' },
				{ property: 'og:title', content: 'Платформа инновационного развития' },
				{
					property: 'og:description',
					content:
						'Сообщество единомышленников, объединённых для создания и продвижения передовых инженерных и технологических решений',
				},
				{ property: 'og:url', content: siteUrl },
				{ property: 'al:android:url', content: siteUrl },
				{ property: 'og:image', content: `${siteUrl}/logo.svg` },
				{ property: 'og:locale', content: 'ru_RU' },
				{ property: 'og:image:width', content: '968' },
				{ property: 'og:image:height', content: '504' },
			],
			link: [
				{ rel: 'canonical', href: siteUrl },
				// { rel: 'apple-touch-icon', sizes: '180x180', href: '/favicons/apple-touch-icon-180x180.png' },
				// { rel: 'icon', sizes: '32x32', type: 'image/png', href: '/favicons/favicon-32x32.png' },
				// { rel: 'icon', sizes: '16x16', type: 'image/png', href: '/favicons/favicon-16x16.png' },
				{ rel: 'shortcut icon', href: '/favicon.ico' },
				// { rel: 'manifest', href: '/manifest.json' },
			],
		},
	},
})
