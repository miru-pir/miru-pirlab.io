import vue from 'eslint-plugin-vue'
import vueParser from 'vue-eslint-parser'

export default [
	{
		ignores: ['node_modules', 'dist'], // Игнорируемые директории
	},
	{
		files: ['**/*.vue'], // Указываем файлы для обработки
		languageOptions: {
			parser: vueParser, // Парсер для Vue
			parserOptions: {
				ecmaVersion: 'latest', // Версия ECMAScript
				sourceType: 'module', // Модульный формат
			},
		},
		plugins: {
			vue, // Плагин для Vue
		},
		rules: {
			...vue.configs.recommended.rules, // Используем рекомендованные правила Vue
		},
	},
]
