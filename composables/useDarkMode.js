export const useDarkMode = () => {
	const isDarkMode = useState('darkMode', () => false) // Глобальное состояние

	const toggleDarkMode = () => {
		isDarkMode.value = !isDarkMode.value
		document.documentElement.classList.toggle('dark', isDarkMode.value)
	}

	return { isDarkMode, toggleDarkMode }
}
